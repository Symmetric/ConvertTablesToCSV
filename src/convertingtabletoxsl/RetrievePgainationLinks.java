/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convertingtabletoxsl;

import java.util.ArrayList;
import org.jsoup.nodes.Document;
import org.jsoup.*;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author dude
 */
public class RetrievePgainationLinks extends Thread{

	private String currentLink;
	private String linkNumber;
	public RetrievePgainationLinks(String link,String linkNumber) {
		System.out.println("Retrieving the links from page number: "+ linkNumber);
		ConvertingTableToXSL.numberOfThread.incrementAndGet();
		this.currentLink =link ;
		this.linkNumber = linkNumber;
		this.start();
	}

	@Override
	public void run() {
		ArrayList<String> linksText = new ArrayList<String>();
		try{
			Document doc = Jsoup.connect(this.currentLink).get();
			Elements els = doc.getElementsByClass("tablePgaination");
			Elements linksElem = els.get(0).getElementsByTag("a");
			for(Element linkElem: linksElem)
			{
				String thisLinkHref = linkElem.attr("href");
				String thisLinkNumber = linkElem.text();
				if(!ConvertingTableToXSL.pagesExtracted.containsKey(thisLinkHref)
					|| !ConvertingTableToXSL.pagesExtracted.get(thisLinkHref))
				{
					new ExtractTableFromPage(thisLinkHref, thisLinkNumber);
					ConvertingTableToXSL.pagesExtracted.put(thisLinkHref, Boolean.TRUE);
				}
			}
			Element linkElem = linksElem.get(linksElem.size()-2);
			String thisLinkHref = linkElem.attr("href");
			String thisLinkNumber = linkElem.text();

			if(!ConvertingTableToXSL.linksVisited.containsKey(thisLinkHref)
				|| !ConvertingTableToXSL.linksVisited.get(thisLinkHref))
			{
				new RetrievePgainationLinks(thisLinkHref, thisLinkNumber);
				ConvertingTableToXSL.linksVisited.put(thisLinkHref, Boolean.TRUE);
			}
			System.out.println("just finished collecting " + linksElem.size()+ " links.");
		}
		catch(Exception ex)
		{
			System.out.println("RetrievePgainationLinks: I am having problem this link: " +this.currentLink);
			System.out.println(ex);

			new RetrievePgainationLinks(this.currentLink, this.linkNumber);
			ConvertingTableToXSL.linksVisited.put(currentLink, Boolean.FALSE);
		}
		finally{
			if(ConvertingTableToXSL.numberOfThread.decrementAndGet() == 0){
				ConvertingTableToXSL.exportToCSV();
			}
		}
	}
}
