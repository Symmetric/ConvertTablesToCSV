/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convertingtabletoxsl;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import org.jsoup.*;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author dude
 */
public class ConvertingTableToXSL {

    /**
     * @param args the command line arguments
     */
	
	public static Map<String,Boolean> linksVisited ;
	public static Map<String,Boolean> pagesExtracted;
	public static ArrayList<ArrayList<String>> tableOfInformation;
	public static AtomicInteger numberOfThread ;
	public static void main(String[] args) {
		// TODO code application logic here
		linksVisited = new HashMap<>();
		pagesExtracted = new HashMap<>();
		tableOfInformation = new ArrayList<>();
		numberOfThread = new AtomicInteger();
		Scanner input = new Scanner(System.in);
		System.out.println("The program started at: " + new Date());
		System.out.println("Enter the first page: ");
		String pageUrl  = input.nextLine();
		String pageNumber = "0";
		new RetrievePgainationLinks(pageUrl, pageNumber);
	}
	public static void exportToCSV(){
		System.out.println("Exporting Array To File");
		BufferedWriter br;
		try{
			br = new BufferedWriter(new FileWriter("myfile.xls"));
			StringBuilder sb = new StringBuilder();
			for (ArrayList<String> element : tableOfInformation) {
				for(String ele : element)
				{
					 sb.append(ele);
					 sb.append("	");
				}
				 sb.append("\n");
			}

			br.write(sb.toString());
			br.close();

		}catch(Exception ex)
		{
			System.out.println("Something Went Wrong!!");
		}
	}
}
