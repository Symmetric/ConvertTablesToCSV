/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convertingtabletoxsl;

import java.util.ArrayList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author dude
 */
public class ExtractTableFromPage extends Thread{

	String currentLink;
	String linkNumber;
	public ExtractTableFromPage(String link,String linkNumber) {
		this.linkNumber = linkNumber;
		ConvertingTableToXSL.numberOfThread.incrementAndGet();
		System.out.println("Parsing Page Number: " + linkNumber);
		this.currentLink = link;
		this.start();
	}

	@Override
	public void run() {
		try{
			ArrayList<ArrayList<String>> rowsInforamtion = new ArrayList<>();
			Document doc = Jsoup.connect(this.currentLink).get();
			Elements tableRows = doc.getElementsByClass("peopleListing").get(0).getElementsByTag("tr");
			System.out.println("Page's" + linkNumber + " content mine!!");
			for(Element row: tableRows)
			{
				ArrayList<String> rowText = new ArrayList<>();
				Elements rowInfos = row.getElementsByTag("td");
				for(Element rowInfo:rowInfos)
				{
					String currentText = "I am in url";
					if(!rowInfo.getElementsByTag("ul").isEmpty()){
						break;
					}
					else if(!rowInfo.getElementsByTag("a").isEmpty())
					{
						currentText = rowInfo.getElementsByTag("a").text();
						rowText.add(rowInfo.getElementsByTag("a").text());

					}
					else{
						currentText = rowInfo.text();
						rowText.add(rowInfo.text());
					}
				}
				rowsInforamtion.add(rowText);
			}
			System.out.println("Finished Extracting "+ rowsInforamtion.size() + " rows");
			ConvertingTableToXSL.tableOfInformation.addAll(rowsInforamtion);
		}
		catch(Exception ex)
		{
			System.out.println("ExtractTableFromPage: I am having problem this link: " +this.currentLink);
			System.out.println(ex);
			new ExtractTableFromPage(this.currentLink, this.linkNumber);
			ConvertingTableToXSL.linksVisited.put(currentLink, Boolean.FALSE);
		}
		finally{
			System.out.println("Number of thread currently running is: "+ConvertingTableToXSL.numberOfThread.get());
			if(ConvertingTableToXSL.numberOfThread.decrementAndGet() == 0){
				ConvertingTableToXSL.exportToCSV();
			}
		}
	}

	
}
